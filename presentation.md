Cryptocurrency
===

# ![100%](img/colored-coins.png)

##### Eine kryptische Ausleitung

###### Fabian Mächler ( fabian.maechler@noser.com )

---

# Die Basis - Kryptowährung

- Eine Kryptowährung ist "digitales Geld" 
- In einer auf einem gemeinsamen Zettel schreiben alle ihre Transaktionen auf (aka Blockchain)
- Ursprüngliches Ziel: ein sicheres, verteiltes, dezentrales Zahlungssystem
  (Die Blockchain kann aber noch viel mehr)

---
# Der Hype "Through the roof"

![100%](img/coindesk-bpi-chart.svg)

https://www.coindesk.com/price/

---
# Aktuelle Situation

- Seit April fliesst viel Geld in Cryptos
- Der Ur-Vater aller Cryptocurrencies Bitcoin hat Probleme
  - Blockgrösse, Mining, Entscheidungsunfähigkeit
- Nach Verunsicherung (BCH, 1. August) erneuter Aufschwung
- Es gibt über 3000 Alt-Coins
  - Starke Ausrichtung auf Bitcoin (beginnt sich zusehends zu ändern)
---

![75%](img/coinmarketcap.png)
https://coinmarketcap.com/

---
# Der Alltag 


---
# Wallets

- Wallets sind Android Apps

---
# Der Alltag

![75%](img/jaxx-wallet.jpg)

---
# Keys & Addressen
- Das Key-Pair ist alles was man braucht:
  - Den Public-Key (Addresse) den man frei verteilen kann. Damit kann jeder Geld in das Wallet überweisen.
  - Den Private-Key muss man schützen. Er ist der Schlüssel um Geld aus dem Wallet zu nehmen.
  - z.B. einfach auf einen Zettel schreiben:

```
  shelter tender tenuous base increase energetic
  chop interest stomach move nippy male
```
---

![100%](img/adress_fabian.png)

---
# Die Hürde 
#### Woher bekomme ich überhaupt Bitcoins?

---
## Jemand gibt einem Coins

oder

## Börsen (Exchange)  

---

## Digital Currency Exchanges (DCEs)

DCEs Handeln mit Cryptowährungen
-  Gewisse wechseln (langweiliges) normales Geld (FIAT) zu Cryptos (wie z.B. Kraken, CoinBase etc)
   -  Müssen (wohl aus geldwäscherischen Gründen) ihre Kunden gut kennen. -> Ausweiskopie notwendig.
-  Andere handeln nur mit Cryptowährungen und können "anonym" benutzt werden (wie z.B. Poloniex, Bitfinex etc.)

---

## Kraken Funding
![100%](img/kraken_fund.png)

---

## Poloniex Trading
![100%](img/poloniex_trade.png)

---

# Präsentation verfügbar unter Git
Hosted "somewhere" in the smoggy Cloud. Username checks out.
https://gitlab.com/unSinn/presentation-crypto.git

For everything else click here:
https://nosercloud.sharepoint.com/SitePages/Vorschlagswesen.aspx

## Also: Cheers to the power of Markdown
Presentation made with Marp https://github.com/yhatt/marp ![50%](img/marp.png)